@if ($errors->any())
    <div class="alert alert-dismissible alert-danger">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li> <i class="icon fas fa-ban"></i>  {!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(Session::has('message'))

    <div class="alert alert-dismissible {{ Session::get('alert-class', 'alert-success') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @if(is_array(Session::get('message') ))
            <ul>
                @foreach(Session::get('message')  as $row)
                    <i class="icon fas fa-check"></i> <li>  {!! $row !!}</li>
                @endforeach
            </ul>
        @else
           <i class="icon fas fa-check"></i>  {!! Session::get('message') !!}
        @endif
    </div>
@endif