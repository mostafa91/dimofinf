@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ $page_description }} </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @include('layouts.messages')

              <form id="quickForm" method="POST" action="{{ url('users') }}">
                 @csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputName1">{{ __('Name') }}</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="{{ __('Name') }}" value="{{ old('name') }}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">{{ __('Email Address') }}</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="{{ __('Email Address') }}" value="{{ old('email') }}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">{{ __('Password') }}</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="{{ __('Password') }}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPasswordConfirmation1">{{ __('Confirm Password') }}</label>
                    <input type="password" name="password_confirmation" class="form-control" id="exampleInputPasswordConfirmation1" placeholder="{{ __('Confirm Password') }}">
                  </div>


                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection 

  @section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
      $(function () {
        $.validator.setDefaults({
          submitHandler: function () {
            $("#quickForm").ajaxForm({url: '{{ url("users") }}', type: 'post'})

    
          }
        });
        $('#quickForm').validate({
          rules: {
            name: {
              required: true,
            },
            email: {
              required: true,
              email: true,
            },
            password: {
              required: true,
              minlength: 5
            },
            password_confirmation : {
                    required: true,
                    minlength : 5,
                    equalTo : "#exampleInputPassword1"
                }
          },
          messages: {
            name: {
              required: "Please enter a User Name",
            },
             email: {
              required: "Please enter a email address",
              email: "Please enter a valid email address"
            },
            password: {
              required: "Please provide a password",
              minlength: "Your password must be at least 5 characters long"
            },
            password_confirmation: {
              required: "Please provide a confirmation password",
              minlength: "Your confirmation password must be at least 5 characters long",
              equalTo :"Your confirmation must be same password"  
            },

          },
          errorElement: 'span',
          errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
          },
          highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
          },
          unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
          }
        });
      });
      </script>


  @endsection