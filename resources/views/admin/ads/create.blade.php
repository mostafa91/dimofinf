@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ $page_description }} </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" method="POST" action="{{ url('ads') }}">
                 @csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputAdTitle1">{{ __('Ad Title') }}</label>
                    <input type="text" name="title" class="form-control" id="exampleInputAdTitle1" placeholder="{{ __('Ad Title') }}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputAdDescription1">{{ __('Ad Description') }}</label>
                    <textarea name="description" class="form-control" id="exampleInputAdDescription1" placeholder="{{ __('Ad Description') }}"></textarea> 
                  </div>
                  <div class="form-group">
                    <label for="exampleInputContactPhone1">{{ __('Contact Phone') }}</label>
                    <input type="text" name="contact_phone" class="form-control" id="exampleInputContactPhone1" placeholder="{{ __('Contact Phone') }}">
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection 

  @section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
      $(function () {
        $.validator.setDefaults({
          submitHandler: function () {
            $("#quickForm").ajaxForm({url: '{{ url("ads") }}', type: 'post'})

    
          }
        });
        $('#quickForm').validate({
          rules: {
            title: {
              required: true,
            },
            description: {
              required: true,
              minlength: 5
            },
            contact_phone: {
              required: true
            },
          },
          messages: {
            title: {
              required: "Please enter a Ad Title",
            },
            description: {
              required: "Please Enter a description",
              minlength: "Your description must be at least 5 characters long"
            },
            contact_phone: "Please enter a contact phone"
          },
          errorElement: 'span',
          errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
          },
          highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
          },
          unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
          }
        });
      });
      </script>


  @endsection