<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple Api Application links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'User_register_successfully' => 'User Register Successfully',

];
