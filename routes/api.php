<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'App\Http\Controllers\api\UserController@register');
Route::post('login', 'App\Http\Controllers\api\UserController@login');
Route::post('resetpassword', 'App\Http\Controllers\api\ForgotPasswordController@forgot_password');
Route::post('password/reset', 'App\Http\Controllers\api\ForgotPasswordController@resetPassword');

Route::middleware('auth:api')->group( function () {
    Route::get('getuserads/{userid}', 'App\Http\Controllers\api\AdsController@getuserAds');
    Route::post('uploadadimage/{id}', 'App\Http\Controllers\api\AdsController@uploadImage');
    Route::resource('ads', App\Http\Controllers\api\AdsController::class);
});
