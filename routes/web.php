<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('oauth/{driver}', 'App\Http\Controllers\Auth\LoginController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'App\Http\Controllers\Auth\LoginController@handleProviderCallback')->name('social.callback');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('ads', 'App\Http\Controllers\admin\AdsController');
    Route::resource('users', 'App\Http\Controllers\admin\UsersController');

});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::resource('users', 'admin\UserController');
//Route::resource('ads', 'App\Http\Controllers\admin\AdsController');

