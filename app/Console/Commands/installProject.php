<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class installProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:project {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'that command will run migraton and seeder automatcally';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call('migrate');
        Artisan::call('db:seed');
        //dd('ddd');
        
    }
}
