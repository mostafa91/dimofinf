<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->user->id,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'A Name is required',
            'email.required' => 'A email is required',
            'email.email' => 'A email is must in email form like example@example.example',
            'email.unique' => 'this email is already registered before',
        ];
    }

}
