<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Http\Requests\StoreAdRequest;
use App\Http\Requests\UpdateAdRequest;
use Illuminate\Support\Facades\Auth;
use Session;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ads = Ad::all();
        $page_title='Ads';
        $page_description='List of All Ads';
        return view('admin.ads.list',compact('page_title', 'page_description','ads'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page_title='Ads';
        $page_description='Create New Ad';
        return view('admin.ads.create',compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdRequest $request)
    {
        //
        $inputs = $request->except(['_token']);
        $inputs['user_id']=\Auth::user()->id;
        $ad = Ad::create($inputs);
        Session::flash('message', 'Ad created Successfully');
        Session::flash('alert-class', ' alert-success');

        return redirect('ads');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        //
        $page_title='Ads';
        $page_description='Show Ad Details';
        return view('admin.ads.show',compact('ad','page_title','page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
        $page_title='Ads';
        $page_description='Edit Ad';
        return view('admin.ads.edit',compact('ad','page_title','page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdRequest  $request
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdRequest $request, Ad $ad)
    {
        //
        $ad->update($request->all());
        Session::flash('message', 'Ad Modified Successfully');
        Session::flash('alert-class', ' alert-success');
        return redirect()->route('ads.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        //
        $ad->delete();
        Session::flash('message', 'Ad deleted Successfully');
        Session::flash('alert-class', ' alert-success');
        return redirect()->route('ads.index');
    }
}
