<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Auth;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        $page_title='Users';
        $page_description='List of All Users';
        return view('admin.users.list',compact('page_title', 'page_description','users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page_title='Users';
        $page_description='Create New User';
        return view('admin.users.create',compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        //
        $inputs = $request->except(['_token']);
        $inputs['password'] = bcrypt($inputs['password']);
        $user = User::create($inputs);
        $user->createToken('MyApp')->accessToken;
        Session::flash('message', 'User created Successfully');
        Session::flash('alert-class', ' alert-success');
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        $page_title='Users';
        $page_description='Show User Details';
        return view('admin.users.show',compact('user','page_title','page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        $page_title='Users';
        $page_description='Edit User';
        return view('admin.users.edit',compact('user','page_title','page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        //
        $user->update($request->all());
        Session::flash('message', 'User Modified Successfully');
        Session::flash('alert-class', ' alert-success');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $user->delete();
        Session::flash('message', 'User deleted Successfully');
        Session::flash('alert-class', ' alert-success');
        return redirect()->route('users.index');
    }
}
