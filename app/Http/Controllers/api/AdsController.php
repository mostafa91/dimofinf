<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Http\Resources\Ad as AdResource;
use Illuminate\Support\Facades\Auth;
use App\Jobs\ProceessFiles;
use Validator;
use Config;
use DB;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ads = Ad::select('id','title',DB::raw("SUBSTR(description, 1, 512)  AS description"))->orderBy('id', 'desc')->paginate(10);

        return response(['ads' => AdResource::collection($ads), 'message' => 'Retrieved successfully'], 200);

        // return $this->sendResponse(AdResource::collection($ads), 'Ads retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
            'contact_phone' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        if($validator->fails()){
            return response(['error' => $validator->errors(), 'Validation Error']);
        }

        $user = Auth::user(); 
        $input['user_id']=$user->id;
        $ad = Ad::create($input);

        return response([ 'ad' => new AdResource($ad), 'message' => 'Created successfully'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objAd = Ad::find($id);
        if (is_null($objAd)) {
            return $this->sendError('Ad not found!');
        }
        $objAd['user_name']=$objAd->user->name;
        unset($objAd['user']);

        return response([ 'ad' => $objAd, 'message' => 'Retrieved successfully'], 200);
    }

    public function uploadImage(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $ad=Ad::find($id);
        if(!$ad)
        {
             return $this->sendError('Ad Or Post Not Found');
        }
        $image=$request->image;
        $fileName = $image->store(Config::get('constants.ADS_IMAGE_PATH'));
        $ad->image=$fileName;
        $ad->save();

        //dispatch(new ProceessFiles ($request->image));
        return response([ 'ad' => new AdResource($ad), 'message' => 'Ad Image  Uploaded successfully'], 200);


    }

    public function getuserAds($userid)
    {
        $arrAds = Ad::where('user_id',$userid)->get();
        return response([ 'ads' => $arrAds, 'message' => 'user Ads Retrieved successfully'], 200);
    }

}
