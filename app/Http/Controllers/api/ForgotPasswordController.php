<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use Validator;

class ForgotPasswordController extends Controller
{
    //
    public function forgot_password(Request $request)
    {
        $input = $request->all();
        $rules = array(
            'email' => "required|email|exists:users,email",
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        } else {
            $response = Password::sendResetLink($input);
            if($response == Password::RESET_LINK_SENT)
            {
                $success=array();
                return $this->sendResponse($success, 'Mail send successfully.');

            }else{
                return $this->sendError('Happening Error when Send Mail Please Try in Different Time.');
            }
        }
        
    }

    public function resetPassword(Request $request){

    $input = $request->only('email','token', 'password', 'password_confirmation');
    $validator = Validator::make($input, [
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed',
    ]);
    if ($validator->fails()) {
        return $this->sendError('Validation Error.', $validator->errors());
    }
    $response = Password::reset($input, function ($user, $password) {
        $user->password = Hash::make($password);
        $user->save();
    });

    if($response == Password::PASSWORD_RESET)
    {
        $success=array();
        return $this->sendResponse($success, 'Password reset successfully.');

    }else{
        return $this->sendError('Happening Error when reset Password Please Try in Different Time.');
    }

}



}
