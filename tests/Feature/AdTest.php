<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Ad;

class AdTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    // public function testAdListedSuccessfully()
    // {

    //     $user= User::factory()->create();
    //     $this->actingAs($user, 'api');

    //     Ad::factory()->create([
    //         "title" => "Test 1",
    //         "description" => "description 1",
    //         "contact_phone" => "phone1",
    //     ]);

    //     Ad::factory()->create([
    //         "title" => "Test 2",
    //         "description" => "description 2",
    //         "contact_phone" => "phone2",
    //     ]);

    //     $this->json('GET', 'api/ads', ['Accept' => 'application/json'])
    //         ->assertStatus(200)
    //         ->assertJson([
    //             "ads" => [
    //                 [
    //                     "id" => 1,
    //                     "title" => "Test 1",
    //                     "description" => "description 1",
    //                 ],
    //                 [
    //                     "id" => 2,
    //                     "title" => "Test 2",
    //                     "description" => "description 2",
    //                 ]
    //             ],
    //             "message" => "Retrieved successfully"
    //         ]);
    // }

    public function testAdCreatedSuccessfully()
    {
        $user= User::factory()->create();
        $this->actingAs($user, 'api');

        $adData = [
            "title" => "Ad Title Test",
            "description" => "Ad description Test",
            "contact_phone" => "phone test",
        ];

        $this->json('POST', 'api/ads', $adData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                "ad" => [
                    "title" => "Ad Title Test",
                    "description" => "Ad description Test",
                    "contact_phone" => "phone test",
                ],
                "message" => "Created successfully"
            ]);
    }

    public function testRetrieveAdSuccessfully()
    {
        $user= User::factory()->create();
        $this->actingAs($user, 'api');

        $ad = Ad::factory()->create([
            "title" => "Susan Wojcicki",
            "description" => "YouTube",
            "contact_phone" => "2014",
        ]);

        $this->json('GET', 'api/ads/' . $ad->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "ad" => [
                    "title" => "Susan Wojcicki",
                    "description" => "YouTube",
                    "contact_phone" => "2014",
                ],
                "message" => "Retrieved successfully"
            ]);
    }

}
