<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class AdFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'description' => $this->faker->text,
            'contact_phone' => $this->faker->numerify('###-###-####'),
            'user_id'=> User::inRandomOrder()->first()->id,
        ];
    }
}
